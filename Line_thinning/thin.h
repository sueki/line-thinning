#ifndef __THIN__
#define __THIN__

#include<stack>
#include<deque>
#include<fstream>
#include <opencv2/opencv.hpp>
#include "Point.h"
using namespace std;

namespace thin{
	void thinning(cv::Mat &src, cv::Mat &dst);
	void extractLine(cv::Mat &src, vector<deque<Point> > &v);
	void showLine(vector<deque<Point> > &v, cv::Mat &dst);
	vector<Point> vectorize(deque<Point> &v);
	vector<vector<Point> > vectorizeAll(vector<deque<Point> > &v);
	double distance(Point a, Point b, Point c);
	void outputLines(vector<vector<Point> > &lines, string ofpath);

	//画像からエッジ抽出、methodは1がDoG
	void extractEdge(cv::Mat &src, cv::Mat &dst, int method = 1);

	//画像から１フレーム分の線を抽出
	vector<vector<Point> > lineFromFrame(cv::Mat &src);

	//(線の集合のvector)から、1フレームごとのエッジのpolylineをofpathに書き出す
	void outputMovie(vector<vector<vector<Point> > > &lines, string ofpath);
}

#endif