#include <cmath>

class Point{
public:
	double x;
	double y;

	Point() : x(0.0), y(0.0) {
	}

	Point(double a) : x(a), y(a) {
	}

	Point(double a, double b){
		x = a;
		y = b;
	}

	Point(const Point &p){
		x = p.x;
		y = p.y;
	}

	double len(){ return hypot(x, y); }
};

Point operator + (const Point & a, const Point & b);

Point operator - (const Point & a, const Point & b);