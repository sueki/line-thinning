#include <opencv2/opencv.hpp>
#include "thin.h"

using namespace std;
string moviePath, outPath;

int main(){
	cin >> moviePath;
	cin >> outPath;
	cv::VideoCapture cap(moviePath);
	cv::Mat frame, grayFrame;
	vector<vector<vector<Point> > > lines_;
	int current_frame = 1;
	while (1){
		//グレーでフレーム読み込み
		cap >> frame;
		if (frame.empty()) break;
		cv::cvtColor(frame, grayFrame, CV_RGB2GRAY);
		lines_.push_back(thin::lineFromFrame(grayFrame));
		cout << current_frame << endl; 
		current_frame++;
		//if (current_frame > 100)break;
	}
	thin::outputMovie(lines_, outPath);

	string srcDir = "ahiru.tif";
	cv::Mat src = cv::imread(srcDir, 0), dst, gaus;
	cv::Mat bilateral(src.size(), CV_8UC1);

	cv::bilateralFilter(src, bilateral, 11, 10.0, 30, 0);

	cv::Mat g1, g2;
	cv::Mat dog(src.size(), CV_8UC1);
	cv::GaussianBlur(src, g1, cv::Size(7, 7), 5);
	cv::GaussianBlur(src, g2, cv::Size(9, 9), 7);
	for (int y = 0; y < src.rows; y++){
		for (int x = 0; x<src.cols; x++){
			dog.at<uchar>(y, x) = ((double)g1.at<uchar>(y, x) - (double)g2.at<uchar>(y, x)) * 20 >70 ? 0 : 255;
		}
	}


	//cv::threshold(gaus, dst, 100,255,cv::THRESH_BINARY);
	cv::adaptiveThreshold(bilateral, dst, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 1.5);

	cv::Mat opened, closed;
	cv::Mat element(3, 3, CV_8U, cv::Scalar::all(255));
	element.at<uchar>(0, 0) = 0;
	element.at<uchar>(0, 2) = 0;
	element.at<uchar>(2, 0) = 0;
	element.at<uchar>(2, 2) = 0;
	cv::Mat thinned;

	cv::morphologyEx(dst, opened, cv::MORPH_OPEN, element, cv::Point(-1, -1), 1);
	cv::morphologyEx(opened, closed, cv::MORPH_CLOSE, element, cv::Point(-1, -1), 1);
	thin::thinning(closed, thinned);

	vector<deque<Point> > points;
	thin::extractLine(thinned, points);
	vector<vector<Point> > lines = thin::vectorizeAll(points);
	thin::outputLines(lines, "lines.txt");
	cv::Mat dstt = thinned.clone();
	thin::showLine(points, dstt);

	cv::imshow("img", src);
	cv::imshow("thres", dst);
	cv::imshow("edge", opened);
	cv::imshow("close", closed);
	cv::imshow("bilateral", bilateral);
	cv::imshow("thinned", thinned);
	cv::imshow("g1", g1);
	cv::imshow("g2", g2);
	cv::imshow("dog", dog);
	cv::waitKey(0);

	cvDestroyAllWindows();
	return 0;
}