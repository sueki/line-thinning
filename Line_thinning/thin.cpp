#include "thin.h"

int rule[] = {
	208,
	88,
	25,
	19,
	22,
	52,
	304,
	400,
	464,
	216,
	89,
	27,
	23,
	54,
	308,
	432,
	180,
	153,
	408,
	240,
	472,
	217,
	91,
	31,
	55,
	310,
	436,
	496,
	409,
	244,
	473,
	219,
	95,
	63,
	311,
	438,
	500,
	504,
	475,
	223,
	127,
	319,
	439,
	502,
	508,
	505,
	509,
	479,
	383,
	503
};

void print_window(int a[25]){
	for (int i = 0; i < 5; i++){
		for (int j = 0; j < 5; j++){
			cout << a[i * 5 + j] << " ";
		}
		puts("");
	}
	puts("");
}

string specialCase[7] = {
	"xxxxxxx0xxx111xx111xxx0xx",
	"xx00xx110xx010xx00xxxxxxx",
	"xxxxxxx00xx010xx011xx00xx",
	"xxxxxx0000x0110x0110x0000",
	"xxxxxxx000x0110x001xxxxxx",
	"xxxxxxx11xx0110xx11xxxxxx",
	"xxxxx000xx0110xx100xxxxxx"
};

bool removable(int a[9]){
	int value = 0;
	for (int i = 0; i < 9; i++){
		value += a[i];
		value <<= 1;
	}
	value >>= 1;
	for (int i = 0; i < 50; i++)if (value == rule[i])return true;

	return false;
}

bool special(int a[25]){
	//print_window(a);
	for (int i = 0; i < 7; i++){
		bool match = true;
		for (int j = 0; j < 25; j++){
			if (specialCase[i][j] != 'x' && (int)(specialCase[i][j] - '0') != a[j]){
				match = false;
				break;
			}
		}
		if (match){
			//puts("match!");
			return true;
		}
	}
	return false;
}

void thin::thinning(cv::Mat &src, cv::Mat &dst){
	int w = src.cols;
	int h = src.rows;
	dst = src.clone();

	int dx[25] = { -1, -1, -1, 0, 0, 0, 1, 1, 1, -2, -2, -2, -2, -2, -1, -1, 0, 0, 1, 1, 2, 2, 2, 2, 2 };
	int dy[25] = { -1, 0, 1, -1, 0, 1, -1, 0, 1, -2, -1, 0, 1, 2, -2, 2, -2, 2, -2, 2, -2, -1, 0, 1, 2 };
	int dx2[25] = { -2, -2, -2, -2, -2, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2 };
	int dy2[25] = { -2, -1, 0, 1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2 };

	int numRemoved = 0;

	do{
		numRemoved = 0;
		for (int y = 0; y < h; y++){
			for (int x = 0; x < w; x++){
				int window[9];
				for (int k = 0; k < 9; k++){
					int nx = x + dx[k];
					int ny = y + dy[k];
					if (nx<0 || nx >= w || ny<0 || ny >= h)window[k] = 0;
					else if (src.at<uchar>(ny, nx)>0)window[k] = 0;
					else window[k] = 1;
				}

				if (src.at<uchar>(y, x)>0)dst.at<uchar>(y, x) = 255;
				else if (removable(window)){
					int specialWindow[25];
					for (int k = 0; k < 25; k++){
						int nx = x + dx2[k];
						int ny = y + dy2[k];
						if (nx < 0 || nx >= w || ny<0 || ny >= h)specialWindow[k] = 0;
						else if (src.at<uchar>(ny, nx)>0)specialWindow[k] = 0;
						else specialWindow[k] = 1;
					}

					if (special(specialWindow))dst.at<uchar>(y, x) = 0;
					else{
						dst.at<uchar>(y, x) = 255;
						numRemoved++;
					}
				}
				else dst.at<uchar>(y, x) = 0;
			}
		}

		src = dst.clone();
	} while (numRemoved > 0);

}

vector<Point> neighbors(cv::Mat &src, int x, int y){
	int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };
	vector<Point> res;
	for (int i = 0; i < 8; i++){
		int nx = x + dx[i], ny = y + dy[i];
		if (src.at<uchar>(ny, nx) == 0)res.push_back(Point(nx, ny));
	}
	return res;
}

vector<Point> live_neighbors(cv::Mat &src, vector<vector<int> > &visited, int x, int y){
	int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };
	vector<Point> res;
	for (int i = 0; i < 8; i++){
		int nx = x + dx[i], ny = y + dy[i];
		if (min(nx, ny)<0 || nx>=src.cols || ny>=src.rows)continue;
		if (src.at<uchar>(ny, nx) == 0 && visited[ny][nx] == 0)res.push_back(Point(nx, ny));
	}
	return res;
}

bool find_lines(cv::Mat &src, vector<vector<Point> > &v){
	int w = src.cols, h = src.rows;
	int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };
	bool find = false;
	for (int y = 0; y < h; y++){
		for (int x = 0; x < w; x++){
			if (src.at<uchar>(y, x) == 0){
				find = true;
				stack<Point> start_points;
				start_points.push(Point(y, x));
				while (!start_points.empty()){
					//dfs
					int tx = start_points.top().x, ty = start_points.top().y;
					start_points.pop();
					if (src.at<uchar>(ty, tx) > 0)continue;

					vector<Point> points;
					points.push_back(Point(tx, ty));
					src.at<uchar>(ty, tx) = 255;
					vector<Point> next_points = neighbors(src, tx, ty);
					while (next_points.size() == 1){
						//dfs loop
						points.push_back(next_points[0]);
						src.at<uchar>(ty, tx) = 255;
						tx = next_points[0].x;
						ty = next_points[0].y;
						next_points.clear();
						next_points = neighbors(src, tx, ty);
					}
					for (int i = 0; i < next_points.size(); i++){
						start_points.push(next_points[i]);
					}
					v.push_back(points);
				}
				break;
			}
		}
		if (find)break;
	}
	return find;
}

Point find_start_point(cv::Mat &src, vector<vector<int> > &visited){
	int w = src.cols, h = src.rows;
	for (int y = 0; y < h; y++){
		for (int x = 0; x < w; x++){
			if (src.at<uchar>(y, x) == 0 && visited[y][x] == 0)return Point(x, y);
		}
	}
	return Point(-1, -1);
}

void show_image(cv::Mat &src, vector<vector<int> > &visited){
	cv::Mat tmp(src.size(), src.type());
	int w = src.cols, h = src.rows;
	for (int y = 0; y < h; y++){
		for (int x = 0; x < w; x++){
			tmp.at<uchar>(y, x) = ((src.at<uchar>(y, x) == 0 && visited[y][x] == 0) ? 0 : 255);
		}
	}
	cv::imshow("img_tmp", tmp);
	cv::waitKey();
}

//(startX, startY)から一本道な線をたどって点列を求める。
//3以上の黒ピクセルを周囲に見つける（か行き止まりの端に当たる）と止まる。
deque<Point> travel(cv::Mat &src, vector<vector<int> > &visited, int startX, int startY){
	deque<Point> res;
	int nx = startX, ny = startY;
	vector<Point> next_points;
	while ((next_points = live_neighbors(src, visited, nx, ny)).size() == 1){
		//今の点を既読にして、次の点に移動する。
		visited[ny][nx] = 1;
		res.push_back(Point(nx, ny));
		nx = next_points[0].x;
		ny = next_points[0].y;
	}
	//neighborが0だったら止まるだけだけど、たくさんあった場合は線群に足すけどvisitedにしない、みたいなことする。
	if (next_points.size() > 1){
		res.push_back(Point(nx, ny));
	}
	return res;
}

void thin::extractLine(cv::Mat &src, vector<deque<Point> > &v){
	int w = src.cols, h = src.rows;
	vector<vector<int> > visited(h, vector<int>(w, 0));

	int dx[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int dy[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };
	bool found = false;

	for (int y = 0; y < h; y++){
		for (int x = 0; x < w; x++){
			//黒いところ以外はもうvisited
			visited[y][x] = (src.at<uchar>(y, x) == 0 ? 0 : 1);
		}
	}

	do{
		Point point = find_start_point(src, visited);
		if (point.x < 0)break;
		visited[point.y][point.x] = 1;
		found = true;
		vector<Point> next_points = live_neighbors(src, visited, point.x, point.y);
		//pointを起点にするのは決まりだけど、道が何本伸びてるかで話が変わってくる。
		if (next_points.size() == 1){
			deque<Point> line = travel(src, visited, next_points[0].x, next_points[0].y);
			line.push_front(point);
			v.push_back(line);
		}
		else if (next_points.size() == 2){
			deque<Point> line1 = travel(src, visited, next_points[0].x, next_points[0].y);
			deque<Point> line2 = travel(src, visited, next_points[1].x, next_points[1].y);
			line1.push_front(point);
			for (deque<Point>::iterator it = line2.begin(); it != line2.end(); it++){
				line1.push_front(*it);
			}
			v.push_back(line1);
		}
		else if (next_points.size() >= 3){
			for (int i = 0; i < next_points.size(); i++){
				deque<Point> line = travel(src, visited, next_points[i].x, next_points[i].y);
				line.push_front(point);
				v.push_back(line);
			}
		}
		//show_image(src, visited);
	} while (found);
}

void thin::showLine(vector<deque<Point> > &v, cv::Mat &dst){
	int w = dst.cols, h = dst.rows;

	for (int y = 0; y < h; y++){
		for (int x = 0; x < w; x++){
			dst.at<uchar>(y, x) = 255;
		}
	}
	int period = 14;

	for (int i = 0; i < v.size(); i++){
		cv::line(dst, cv::Point(v[i].front().x, v[i].front().y), cv::Point(v[i].back().x, v[i].back().y), cv::Scalar(128));
		for (int j = 0; j + period < v[i].size(); j += period){
			if (j + period < v[i].size() - 1)cv::line(dst, cv::Point(v[i][j].x, v[i][j].y), cv::Point(v[i][j + period].x, v[i][j + period].y), cv::Scalar(0));
			else cv::line(dst, cv::Point(v[i][j].x, v[i][j].y), cv::Point(v[i].back().x, v[i].back().y), cv::Scalar(0));
		}
		if (period >= v[i].size())cv::line(dst, cv::Point(v[i].front().x, v[i].front().y), cv::Point(v[i].back().x, v[i].back().y), cv::Scalar(0));
	}
	//cv::imshow("dstt", dst);
	//cv::waitKey();
}

int next_point(int from, vector<int> &used, int size){
	for (int i = from + 1; i<size; i++){
		if (used[i]>0)return i;
	}
	return size - 1;
}

//直線ABと点Cの距離
double thin::distance(Point a, Point b, Point c){
	Point ab, ac;
	//cout << " ( " << a.x << " , " << a.y << " ) ";
	//cout << " ( " << b.x << " , " << b.y << " ) ";
	//cout << " ( " << c.x << " , " << c.y << " ) " << endl;
	ac = c - a;
	ab = b - a;
	double abac = ab.x*ac.x + ab.y*ac.y;

	if (ab.len() == 0)return ac.len();
	double hi = abac / (ab.len()*ab.len());
	if (hi < 0)return ac.len();
	else if (hi>1)return (b - c).len();

	Point h(a.x + ab.x*hi, a.y + ab.y*hi);
	return (h - c).len();
}

double maxdist(deque<Point> &v, vector<int> &used, int &maxp){
	int sz = v.size();
	maxp = 0;
	double maxdist = 0, tmpdist;
	Point a = v[0], b;

	//find next vertex
	int next = next_point(0, used, sz);
	b = v[next];

	//今のpolylineの一本一本についてみていく
	for (int i = 1; i < sz - 1; i++){
		//今の線の終点まで来たら次の線に移行
		if (i == next){
			a = b;
			next = next_point(next, used, sz);
			b = v[next];
			continue;
		}

		tmpdist = thin::distance(a, b, v[i]);
		if (tmpdist > maxdist){
			maxp = i;
			maxdist = tmpdist;
		}
	}

	return maxdist;

}

vector<Point> thin::vectorize(deque<Point> &v){
	vector<Point> res;

	double lim = 1.5;

	int sz = v.size();
	vector<int> used(sz, 0);
	used[0] = used[sz - 1] = 1;
	int newp = 0;

	while (true){
		if (maxdist(v, used, newp) < lim)break;
		used[newp] = 1;
	}

	for (int i = 0; i<sz; i++){
		if (used[i]>0)res.push_back(v[i]);
	}
	return res;
}

void showVectorizedLines(vector<vector<Point> > &lines){
	int w = 500, h = 500;
	cv::Mat dst(cv::Size(w, h), CV_8UC1);

	for (int y = 0; y < h; y++){
		for (int x = 0; x < w; x++){
			dst.at<uchar>(y, x) = 255;
		}
	}

	for (int i = 0; i < lines.size(); i++){
		for (int j = 0; j + 1 < lines[i].size(); j++){
			cv::line(dst, cv::Point(lines[i][j].x, lines[i][j].y), cv::Point(lines[i][j + 1].x, lines[i][j + 1].y), cv::Scalar(128));
		}
	}

	for (int i = 0; i < lines.size(); i++){
		for (int j = 0; j < lines[i].size(); j++){
			cv::circle(dst, cv::Point(lines[i][j].x, lines[i][j].y), 3, cv::Scalar(0));
		}
	}
	//cv::imshow("dstt2", dst);
	//cv::waitKey();
}

vector<vector<Point> > thin::vectorizeAll(vector<deque<Point> > &v){
	vector<vector<Point> > res;
	for (int i = 0; i < v.size(); i++){
		res.push_back(thin::vectorize(v[i]));
	}
	showVectorizedLines(res);
	return res;
}

void thin::outputLines(vector<vector<Point> > &lines, string ofpath){
	ofstream ofs(ofpath);
	ofs << lines.size()<<endl;
	for (int i = 0; i < lines.size(); i++){
		ofs << lines[i].size() << endl;
		for (int j = 0; j < lines[i].size(); j++){
			ofs << lines[i][j].x << ",";
			if(j<lines[i].size()-1)ofs << lines[i][j].y << ",";
			else ofs << lines[i][j].y << endl;
		}
	}
}

void thin::extractEdge(cv::Mat &src, cv::Mat &dst, int method){
	cerr << "thin::extractEdge ... ";

	//method == 1 を前提にして書きます
	int kernelSize = 5;
	double ratio = 0.6;
	uchar thres = 2;

	cv::Mat g1(src.size(), src.type());
	cv::Mat g2(src.size(), src.type());
	dst = cv::Mat(src.size(), src.type());

	cv::GaussianBlur(src, g1, cv::Size(0, 0), kernelSize);
	cv::GaussianBlur(src, g2, cv::Size(0, 0), (int)(kernelSize*ratio));

	for (int y = 0; y < src.rows; y++){
		for (int x = 0; x < src.cols; x++){
			dst.at<uchar>(y, x) = abs((g1.at<uchar>(y, x) - g2.at<uchar>(y, x)) >= thres ? (uchar)0 : (uchar)255);
		}
	}
	cerr << "complete." << endl;
}

//extract edge -> thinning -> vectorize
vector<vector<Point> > thin::lineFromFrame(cv::Mat &src){
	cv::Mat edge, thinned;
	thin::extractEdge(src, edge);
	//cv::imshow("edge", edge);
	//cv::waitKey();

	thin::thinning(edge, thinned);

	//cv::imshow("thin", thinned);
	//cv::imshow("edge", edge);
	//cv::waitKey();

	vector<deque<Point> > points;
	thin::extractLine(thinned, points);
	return thin::vectorizeAll(points);
}

void thin::outputMovie(vector<vector<vector<Point> > > &lines, string ofpath){
	ofstream ofs(ofpath);
	ofs << lines.size() << endl;
	for (int i = 0; i < lines.size(); i++){
		ofs << lines[i].size() << endl;
		for (int j = 0; j < lines[i].size(); j++){
			ofs << lines[i][j].size() << endl;
			for (int k = 0; k < lines[i][j].size(); k++){
				ofs << lines[i][j][k].x << ",";
				if (k < lines[i][j].size() - 1)ofs << lines[i][j][k].y << ",";
				else ofs << lines[i][j][k].y << endl;
			}
		}
	}
}